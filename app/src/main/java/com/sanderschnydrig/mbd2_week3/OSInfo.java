package com.sanderschnydrig.mbd2_week3;

/**
 * Created by Sander on 6-3-2015.
 */
public class OSInfo {

    public static final String[] NAMES =
    {
        "Android",
        "iOS",
        "Windows"
    };

    public static final String[] CONTENT =
    {
            "Android Info",
            "iOS Info",
            "Windows Info"
    };
}
